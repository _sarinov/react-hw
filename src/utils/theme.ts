import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#e7e7e7',
    },
    secondary: {
      main: '#f50057',
    },
  },
});

export default theme;