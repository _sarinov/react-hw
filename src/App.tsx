import './App.css';
import Customer from 'features/customer';
import { store } from './app/store';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@material-ui/core';
import theme from 'utils/theme';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <CssBaseline>
        <Customer/>
        </CssBaseline>
      </Provider>
    </ThemeProvider>

  );
}

export default App;
