export interface Customer {
    name: string;
    impression: number | null;
    conversion: number | null;
    attributeMatches: number | null; 
    conversionRate: number | null;
    avgFrequency: number | null;
    avgTimeToConversion: number | null;
    director: string;
    startDate: string;
    endDate:  string;
    isFavorite?: boolean;
  }