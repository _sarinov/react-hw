import customerSaga from 'features/customer/customerSaga';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([customerSaga()]);
}
