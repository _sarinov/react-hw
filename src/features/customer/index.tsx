import React, { useEffect } from 'react'
import CustomerTable from './components/CustomerTable'
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { customerActions, selectCustomerList } from './customerSlice';
import { Box } from '@mui/material';
import { Customer } from 'models';

const Customers = () => {
    const dispatch = useAppDispatch();
    const customerList = useAppSelector(selectCustomerList);
    useEffect(() => {
        dispatch(customerActions.fetchCustomerList());
      }, [dispatch]);
 
    const handleSubmit = (customer: Customer) => {
        dispatch(
            customerActions.addCustomer(customer)
        );
    };

    return (
        <Box style={{margin: '20px'}}>
            <CustomerTable customerList={customerList} onSubmit={handleSubmit}/>
        </Box>
    )
}

export default Customers