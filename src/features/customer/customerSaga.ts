import { Customer } from 'models';
import { call, put, takeLatest } from 'redux-saga/effects';
import { customerActions } from './customerSlice';
import data from 'data/customer.json';

 function getCustomers() {
   const result: Customer[] = data
   return result
  }

function* fetchCustomerList() {
  try {
    const response: Customer[] = yield call(getCustomers);
    yield put(customerActions.fetchCustomerListSuccess(response));
  } catch (error) {
    console.log('Failed to fetch customer list', error);
    yield put(customerActions.fetchCustomerListFailed(''));
  }
}

export default function* customerSaga() {
  yield takeLatest(customerActions.fetchCustomerList.type, fetchCustomerList);

}
