import {
    Box,
    FormControl,
    Grid,
    OutlinedInput,
  } from '@material-ui/core';
import Tabs from '@mui/material/Tabs';
import { styled } from '@mui/material/styles';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Tab from '@mui/material/Tab';
import { Search } from '@mui/icons-material';
import React, { useState, Dispatch, SetStateAction } from 'react';
  
  interface CustomerFiltersProps {
    filter: any;
    setFilter: Dispatch<SetStateAction<any>>;
  }

  interface StyledTabProps {
    label: string;
    value: string;
  }
  interface StyledTabsProps {
    children?: React.ReactNode;
    value: string;
    onChange: (event: React.SyntheticEvent, newValue: string) => void;
  }
  
  const StyledTabs = styled((props: StyledTabsProps) => (
    <Tabs
      {...props}
      style={{
        width: 'auto',
        margin: '10px 0'
    }}
    />
  ))({
    '& .MuiTabs-scroller': {
        border: '1px solid #e7e7e7',
        borderRadius: '5px',
    }, 
    '& .MuiTabs-indicator': {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent',
    },
  });

  const StyledTab = styled((props: StyledTabProps) => (
  <Tab  {...props} />
    ))(({ theme }) => ({
    color: '#505050',
    '&.Mui-selected': {
        color: '#FF00CC',
        fontWeight: 500,
        backgroundColor: '#FFCCFF',
        border: 'none',
    },
    '&.Mui-indicator': {
        backgroundColor: 'none',
    }
    }));

  const CustomerFilters = ({
    filter,
    setFilter
  }: CustomerFiltersProps) => {
  const [value, setValue] = useState('false');

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    console.log(newValue);
    setValue(newValue);
    setFilter({...filter, isFavorite: newValue})
  };
  
    return (
      <Box style={{margin: '20px'}}>
        <Grid alignItems='center' justifyContent='space-between' container>
          <Grid item xs={2} >
            <FormControl  variant="outlined" size="small">          
                <StyledTabs value={value} onChange={handleChange} aria-label="nav tabs example">
                    <StyledTab label="All" value="false" />
                    <StyledTab label="Favorite" value="true" />
                </StyledTabs>       
            </FormControl>
          </Grid>
          <Grid item xs >
            <FormControl  variant="outlined" size="small">
                <OutlinedInput
                    startAdornment={<Search />}
                    defaultValue={''}
                />
            </FormControl>
         </Grid>
        <Grid item xs="auto">
          <FormControl variant="outlined" size="small" >
            <Select
              style={{width: 250, height: 40}}
              value={'year'}
              placeholder='Last year'
              >
                <MenuItem value={'year'}>Last year</MenuItem>
                <MenuItem value={'month'}>Last month</MenuItem>
                <MenuItem value={'week'}>Last week</MenuItem>
              </Select>
          </FormControl>
        </Grid> 
  
        </Grid>
      </Box>
    );
  };
  
  export default CustomerFilters;
  