
import { Box, Button, CircularProgress } from '@material-ui/core';
import { Switch, Typography } from '@mui/material';
import { ChevronLeft } from '@mui/icons-material';
import {makeStyles } from '@material-ui/core'

import 
  InputField
 from 'components/InputField';
import { Customer } from 'models';
import React from 'react';
import { useForm } from 'react-hook-form';
import HorizontalLinearStepper from 'components/CustomStepper';

interface CustomerFormProps {
  initialValues?: Customer;
  onSubmit?: (formValues: Customer) => void;
  closeForm: (open: boolean) => void
}

const useStyles = makeStyles((theme) => ({
  back: {
    display: 'flex',
    alignItems: 'center',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    padding: '40px 70px',
  },
  box: {
    margin: 20,
    minWidth: 600
  },
  minBox: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}));

const initialValues: Customer = {
    name: '',
    impression: null,
    conversion: 4,
    attributeMatches: 123123,
    conversionRate: null,
    avgFrequency: 3.4,
    avgTimeToConversion:  null,
    director: 'Sara',
    startDate: `${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()}`,
    endDate: `${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()}`
} 

const CustomerForm = ({  onSubmit, closeForm }: CustomerFormProps) => {
  const classes = useStyles();
  const {
    control,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<Customer>({
    defaultValues: initialValues 
  });

  const handleFormSubmit = async (formValues: Customer) => {
    try {
      await onSubmit?.(formValues);
      closeForm(false)
    } catch (error) {
    }
  };
  return (
    <Box minWidth={600} className={classes.box}>
        <Typography style={{cursor: 'pointer'}} fontSize={20} color={'primary'} variant="caption" className={classes.back} onClick={() => closeForm(false)}>
          <ChevronLeft /> Back
        </Typography>
        <Box style={{padding: '0 70px'}}>
          <Typography fontSize={26} fontWeight={500} color={'#595959'} marginBottom={2} marginTop={2}>
            Create New Customer
          </Typography>

          <HorizontalLinearStepper/>
          <Typography fontSize={24} color={'#595959'}  marginTop={3} fontWeight={500}>
            Alerts
          </Typography>
        </Box>    
      <form onSubmit={handleSubmit(handleFormSubmit)}  className={classes.container}>
        <InputField name="name" control={control} label="Customer" />
        <InputField name="conversionRate" control={control} label="Low Conversion Rate" />     
        <InputField name="avgTimeToConversion" control={control} label="Average Time To Conversion" />
        <InputField name="impression" control={control} label="Average Impressions" />
        <Box style={{margin: '20px 0'}}>
          <Typography color={'#595959'} fontWeight={500}>No data</Typography>
          <Box className={classes.minBox}>
            <Typography color={'#595959'}>impressions</Typography>
            <Switch defaultChecked color='secondary'></Switch>
          </Box>
          <Box className={classes.minBox}>
            <Typography color={'#595959'} >converstions</Typography>
            <Switch color='secondary'></Switch>
          </Box>
        </Box>
        <Box style={{margin: '20px 0'}}>
          <Typography color={'#595959'} fontWeight={500}>Low utilization</Typography>
          <Box className={classes.minBox}>
            <Typography color={'#595959'}>impressions</Typography>
            <Switch defaultChecked color='secondary'></Switch>
          </Box>
          <Box className={classes.minBox}>
            <Typography color={'#595959'} >converstions</Typography>
            <Switch color='secondary'></Switch>
          </Box>
        </Box>
        <Box>
          <Button
            style={{marginTop: '20px'}}
            fullWidth
            type="submit"
            variant="contained"
            color="secondary"
            disabled={isSubmitting}
          >
            {isSubmitting && <CircularProgress size={16} color="secondary" />}{' '}
            &nbsp;Create New Customer
          </Button>
        </Box>
      </form>
    </Box>
  );
};

export default CustomerForm;
