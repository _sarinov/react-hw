import {
Paper,
Table,
TableBody,
TableContainer,
TableHead,
TableRow,
Button,
} from '@mui/material'
import { styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import Drawer from '@mui/material/Drawer';
import { Customer } from 'models'
import React, { useState, useEffect } from 'react';
import { capitalizeString } from 'utils';
import CustomerFilters from './CustomerFilter';
import CustomerForm from './CustomerForm';
import { ArrowDownward, ArrowUpward } from '@mui/icons-material';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.root}`]: {
        borderBottom: "1px solid #e7e7e7",
        color: '#505050',
        height: '50px',
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  export interface CustomerTableProps {
    customerList: Customer[];
    onSubmit: (formValues: Customer) => void;
  }
  
  const CustomerTable = ({
    customerList,
    onSubmit,
  }: CustomerTableProps) => {
    const [open, setOpen] = useState(false);
    const [order, setOrder] = useState('asc');
    const [filteredList, setFilteredList] = useState<Customer[]>(customerList)
    const [hoveredCustomer, setHoveredCustomer] = useState(-1)
    const [filter, setFilter] = useState<any>({isFavorite: false})
    const handleRowHover = (index: number) =>{
        setHoveredCustomer(index)
    }

    const toggleDrawer = (open: boolean) =>  {
      setOpen(open)
    };

    const handelSort = () => {
      const tempList = [...filteredList];
      if(order === 'asc')
        tempList.sort((a, b) => new Date(a.startDate).getTime() - new Date(b.startDate).getTime());
      else
        tempList.sort((a, b) =>  new Date(b.startDate).getTime() -  new Date(a.startDate).getTime());
      setFilteredList(tempList)
      setOrder(order === 'asc' ? 'desc' : 'asc')
    }

    useEffect(() => {
        const isFavorite = filter.isFavorite === 'true' ? true : false
       setFilteredList(isFavorite  ? customerList.filter(c => c.isFavorite === isFavorite) : customerList) 
    }, [customerList, filter])

  
    return (
      <>
       <Paper sx={{ width: '100%', mb: 2 }}>
        <CustomerFilters filter={filter} setFilter={setFilter}/>
        <TableContainer component={Paper}>
          <Table  size="small" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell onClick={handelSort}>
                 
                  Start Date {order === 'asc' ? <ArrowDownward fontSize='small'/> : <ArrowUpward fontSize='small'/>}</TableCell>
                <TableCell>End Date</TableCell>
                <TableCell>Customer</TableCell>
                <TableCell>Impression</TableCell>
                <TableCell>Conversion</TableCell>
                <TableCell>Attribute matches</TableCell>
                <TableCell>Conversion rate</TableCell>
                <TableCell>Avg. frequency</TableCell>
                <TableCell>Avg. time to Conversion</TableCell>
                <TableCell >Director</TableCell>
              </TableRow>
            </TableHead>
  
            <TableBody>
              {filteredList.map((customer, index) => (
                <TableRow key={index} 
                    onMouseEnter={e => handleRowHover(index)} 
                    onMouseLeave={e => setHoveredCustomer(-1)}>
                  <StyledTableCell>{customer.startDate.toString()}</StyledTableCell>
                  <StyledTableCell>{customer.endDate.toString()}</StyledTableCell>
                  <StyledTableCell>{capitalizeString(customer.name ?? '')}</StyledTableCell>
                  <StyledTableCell>{customer.impression}</StyledTableCell>
                  <StyledTableCell>{customer.conversion}</StyledTableCell>
                  <StyledTableCell>{customer.attributeMatches}</StyledTableCell>
                  <StyledTableCell>{customer.conversionRate}</StyledTableCell>
                  <StyledTableCell>{customer.avgFrequency}</StyledTableCell>
                  <StyledTableCell>{customer.avgTimeToConversion}</StyledTableCell>
                  <StyledTableCell >{hoveredCustomer === index ? 
                    <Button  onClick={() => toggleDrawer(true)} variant="contained" color="secondary">+ Create customer</Button> 
                : customer.director}
                    <Drawer
                        anchor={'right'}
                        open={open}
                        onClose={() => toggleDrawer(false)}
                    >
                        <CustomerForm 
                          initialValues={customer}
                          closeForm={toggleDrawer}
                          onSubmit={onSubmit} 
                        />
                    </Drawer>
                </StyledTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Stack style={{margin: '10px'}} alignItems={'flex-end'}>
            <Pagination count={10} showFirstButton showLastButton />
          </Stack>
        </TableContainer>
        </Paper>
  
      </>
    );
  };
  
  export default CustomerTable;
  