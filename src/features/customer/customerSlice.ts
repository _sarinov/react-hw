import { RootState } from './../../app/store';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Customer } from 'models';

export interface CustomerState {
  loading?: boolean;
  list: Customer[];
}

const initialState: CustomerState = {
  loading: false,
  list: []
};

const customerSlice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    fetchCustomerList(state) {
      state.loading = true;
    },
    addCustomer(state, action: PayloadAction<Customer>) {
      state.list = [...state.list, action.payload];
    },
    fetchCustomerListSuccess(
      state,
      action: PayloadAction<Customer[]>
    ) {      
      state.list = action.payload;
    
      state.loading = false;
    },
    fetchCustomerListFailed(state, action: PayloadAction<string>) {
      state.loading = false;
    },
   
  },
});

// Actions
export const customerActions = customerSlice.actions;

// Selectors
export const selectCustomerLoading = (state: RootState) => state.customer.loading;
export const selectCustomerList = (state: RootState) => state.customer.list;

const customerReducer = customerSlice.reducer;
export default customerReducer;
