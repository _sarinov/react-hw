import * as React from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';

const steps = ['Details', 'Configurations', 'Tags', 'Alerts'];  

export default function HorizontalLinearStepper() {
  return (
    <Box sx={{ width: '100%' }}>
      <Stepper activeStep={3} >
        {steps.map((label, index) => {
          const stepProps: { completed?: boolean } = {};
          const labelProps: {
            optional?: React.ReactNode;
          } = {};
          return (
            <Step  key={label} {...stepProps}>
                <StepLabel 
                    {...labelProps} 
                    // StepIconComponent={<CheckBoxOutlineBlank/>}
                    StepIconProps={{sx: {"&.Mui-completed": {color: '#f50057'},  borderRadius: 0}}}>{label}
                </StepLabel>
            </Step>
          );
        })}
      </Stepper>
    
     
    </Box>
  );
}