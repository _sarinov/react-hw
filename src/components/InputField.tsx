import { InputLabel, TextField, Box } from '@material-ui/core';
import React, { InputHTMLAttributes } from 'react';
import { Control, useController } from 'react-hook-form';

interface InputFieldProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  control: Control<any>;
  label?: string;
}

const InputField = ({
  name,
  control,
  label,
  ...inputProps
}: InputFieldProps) => {
  const {
    field: { value, onChange, onBlur, ref },
    fieldState: { invalid, error },
  } = useController({
    name,
    control,
  });

  return (
    <Box style={{width: '100%'}}>
        <InputLabel style={{color: '#595959', fontWeight: 500}}  id={name}>{label}</InputLabel>
        <TextField
            id={name}
            fullWidth
            size="small"
            margin="normal"
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            variant="outlined"
            inputRef={ref}
            error={invalid}
            helperText={error?.message}
            inputProps={inputProps}
        />
    </Box>
   
  );
};

export default InputField